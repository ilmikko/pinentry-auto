package pinauto

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
)

const (
	SOCKET_FILE = "/tmp/pinentry-auto.sock"
)

func (p *Pinauto) SetupSocket(pins []string) error {
	// This effectively kills any previous processes of pinentry-auto as the socket is accepted.
	p.SocketPIN()

	if err := os.Remove(SOCKET_FILE); err != nil && !os.IsNotExist(err) {
		return err
	}

	log.Printf("Socket listening with %d PIN(s)...", len(pins))
	l, err := net.Listen("unix", SOCKET_FILE)
	if err != nil {
		return err
	}

	// We cannot add send of the pins at once, as a single pinentry process
	// will only get one pin from the user (enforced by pinentry-auto)
	for i, pin := range pins {
		fd, err := l.Accept()
		if err != nil {
			return err
		}
		fmt.Fprintf(fd, "%s\n", pin)
		fd.Close()
		log.Printf("%d PIN(s) remaining.", len(pins)-i-1)
	}

	log.Printf("Pinentry-auto socket finished.")
	return nil
}

func (p *Pinauto) SocketPIN() (string, error) {
	c, err := net.Dial("unix", SOCKET_FILE)
	if err != nil {
		return "", err
	}
	defer c.Close()

	s := bufio.NewScanner(c)
	for s.Scan() {
		return s.Text(), nil
	}

	return "", nil
}
