package pinentry

import "strings"

func (p *Pinentry) Info(text string) {
	fields := strings.Fields(text)
	info, _ := fields[0], strings.Join(fields[1:], " ")
	switch info {
	case "pid":
		p.AckPID()
	case "version":
		p.AckVersion()
	case "flavor":
		p.AckFlavor()
	case "ttyinfo":
		p.AckTTYInfo()
	default:
		p.AckFail()
	}
}

func (p *Pinentry) Option(text string) {
	fields := strings.Split(text, "=")
	option, _ := fields[0], strings.Join(fields[1:], "=")
	switch option {
	default:
		// Let's pretend we did whatever they asked us to do.
		p.AckSuccess()
	}
}
