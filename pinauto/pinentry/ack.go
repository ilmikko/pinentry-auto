package pinentry

import (
	"fmt"
	"os"
)

func (p *Pinentry) AckFailMsg(s string) {
	fmt.Printf("ERR %s\n", s)
}

func (p *Pinentry) AckFail() {
	p.AckFailMsg("")
}

func (p *Pinentry) AckFlavor() {
	fmt.Printf("D %s\n", FLAVOR)
	p.AckSuccess()
}

func (p *Pinentry) AckPID() {
	fmt.Printf("D %d\n", os.Getpid())
	p.AckSuccess()
}

func (p *Pinentry) AckSuccessMsg(s string) {
	fmt.Printf("OK %s\n", s)
}

func (p *Pinentry) AckSuccess() {
	p.AckSuccessMsg("")
}

func (p *Pinentry) AckTTYInfo() {
	fmt.Printf("D - - -\n")
	p.AckSuccess()
}

func (p *Pinentry) AckVersion() {
	fmt.Printf("D %s\n", VERSION)
	p.AckSuccess()
}
