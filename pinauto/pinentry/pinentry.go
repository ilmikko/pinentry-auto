package pinentry

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"../../config"
)

const (
	VERSION = "1.0.0"
	FLAVOR  = "curses:curses"
)

type Pinentry struct {
	cfg *config.Config
	pin string
}

func (p *Pinentry) Listen(pin string) error {
	p.pin = pin
	p.AckSuccessMsg(fmt.Sprintf("I am pinentry-auto v%s", VERSION))
	r := bufio.NewReader(os.Stdin)
	for {
		text, err := r.ReadString('\n')
		if err != nil {
			return err
		}
		fields := strings.Fields(text)
		p.cfg.DebugLog("Received: %q", text)
		cmd, rest := fields[0], strings.Join(fields[1:], " ")
		switch cmd {
		case "BYE":
			p.AckSuccess()
			return nil
		case "CONFIRM":
			fmt.Printf("ASSUAN_Not_Confirmed\n")
		case "GETPIN":
			if err := p.GetPIN(); err != nil {
				p.cfg.DebugLog("GetPIN failed: %v", err)
				p.AckFail()
			} else {
				p.AckSuccess()
			}
			return nil
		case "GETINFO":
			p.Info(rest)
		case "SETDESC":
			p.SetDesc(rest)
		case "SETKEYINFO":
			p.AckSuccess()
		case "SETOK":
			p.SetOK(rest)
		case "SETERROR":
			p.SetError(rest)
		case "SETPROMPT":
			p.SetPrompt(rest)
		case "OPTION":
			p.Option(rest)
		default:
			p.AckSuccess()
		}
	}
}

func New(cfg *config.Config) *Pinentry {
	p := &Pinentry{
		cfg: cfg,
	}
	return p
}
