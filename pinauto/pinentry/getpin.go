package pinentry

import "fmt"

func (p *Pinentry) GetPIN() error {
	if p.pin == "" {
		p.AckFail()
		return fmt.Errorf("Empty PIN")
	}

	fmt.Printf("D %s\n", p.pin)
	p.pin = ""
	p.AckSuccess()
	return nil
}
