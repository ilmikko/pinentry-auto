package pinentry

import (
	"bufio"
	"io"
	"os"
	"os/exec"
)

const (
	BUFFER_SIZE = 2048
)

func (p *Pinentry) Forward(pinentry string) error {
	p.cfg.DebugLog("Forwarding to %q...", pinentry)
	cmd := exec.Command(pinentry, os.Args[1:]...)

	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout

	inpipe, err := cmd.StdinPipe()
	if err != nil {
		return err
	}

	if err := cmd.Start(); err != nil {
		return err
	}

	r := bufio.NewReader(os.Stdin)
	for {
		buf := make([]byte, BUFFER_SIZE)
		n, err := r.Read(buf)
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		p.cfg.DebugLog("WROTE: %q", buf[0:n])
		if _, err := inpipe.Write(buf[0:n]); err != nil {
			return err
		}
	}
	return nil
}
