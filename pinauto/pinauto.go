package pinauto

import (
	"io"
	"os"

	"../config"
	"./pinentry"
)

type Pinauto struct {
	cfg      *config.Config
	Pinentry *pinentry.Pinentry
	debugLog io.Writer
	socket   *os.File
}

func New(cfg *config.Config) *Pinauto {
	p := &Pinauto{
		cfg: cfg,
	}

	return p
}

func Start(cfg *config.Config) error {
	p := New(cfg)
	p.Pinentry = pinentry.New(cfg)

	if !cfg.Socket {
		// If there is a socket with a PIN waiting, we are being automated.
		// Act as a dummy pinentry and reply with the PIN(s) supplied over the socket.
		pin, err := p.SocketPIN()
		if err != nil {
			return p.Pinentry.Forward(cfg.Pinentry)
		}
		return p.Pinentry.Listen(pin)
	}

	return p.SetupSocket(cfg.Pins)
}
