# pinentry-auto

Automated Pinentry program to simplify unattended password input to programs such as GPG.
This can be used to setup smart cards, for example by running a simple bash script:

```
(
	echo "$FACTORY_ADMIN_PIN" # Insert old PIN
	echo "$ADMIN_PIN"         # Insert new PIN
	echo "$ADMIN_PIN"         # Insert new PIN (repeat)
) | pinentry-auto --socket &
PINENTRY=$!;
echo "Pinentry PID $PINENTRY";
(
	(
		send "admin"              # Enable admin commands
	)
	(
		send "passwd"             # Change PINs
		send "3"                  # Change Admin PIN
		# ... here GPG will call pinentry before consulting STDIN ...
		send "Q"                  # Close password menu
	)
	(
		send "quit"               # Quit GPG
	)
) | gpg --command-fd 0 --status-fd 0 --edit-card;
kill $PINENTRY; # In case all PINs were not consumed for some reason, kill the pinentry socket.
```

# Configuration

The only configuration is to tell gpg-agent to use pinentry-auto as the default pinentry program.

```
# In ~/.gnupg/gpg-agent.conf
pinentry-program /usr/bin/pinentry-auto
```

Don't worry - if pinentry-auto is not able to connect to the socket, it falls back to the default pinentry program.
That is, if there is no script running, pinentry-auto will show the PIN display the same way as `/bin/pinentry` does.