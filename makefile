GITPATH=gitlab.com/ilmikko/pinentry-auto

build:
	mkdir -p /tmp/go/src/$(shell dirname $(GITPATH))
	test -L /tmp/go/src/$(GITPATH) || ln -sf $(shell pwd) /tmp/go/src/$(GITPATH)
	GOPATH="/tmp/go" go build -o bin/pinentry-auto pinentry-auto.go
	@rm -rf /tmp/go
