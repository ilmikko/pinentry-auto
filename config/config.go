package config

import (
	"bufio"
	"flag"
	"io"
	"os"
)

var (
	flags = flag.NewFlagSet("flags", flag.ContinueOnError)
)

type Config struct {
	DebugSocket io.Writer
	Socket      bool
	Pinentry    string
	Pins        []string
}

var (
	flagDebug    = flags.Bool("debug", false, "Run pinentry-auto in debug mode")
	flagPin      = flags.String("passphrase", "", "Provide a single passphrase. Use STDIN for queuing multiple passphrases.")
	flagPinentry = flags.String("pinentry", "/usr/bin/pinentry", "Which pinentry to forward socket to")
	flagSocket   = flags.Bool("socket", false, "Run pinentry-auto in socket mode")

	flagDisplay = flags.String("display", "", "Unused. Added for compatibility, forwarded to pinentry if needed.")
)

func readPinsFromStdin() []string {
	st := []string{}

	// Nothing to read
	if stat, _ := os.Stdin.Stat(); stat.Mode()&os.ModeCharDevice != 0 {
		return st
	}

	s := bufio.NewScanner(os.Stdin)

	for s.Scan() {
		st = append(st, s.Text())
	}

	return st
}

func New() *Config {
	cfg := &Config{}

	if *flagDebug {
		cfg.DebugSocket = cfg.DebugMode()
	}
	cfg.Socket = *flagSocket
	if cfg.Socket {
		cfg.Pins = readPinsFromStdin()
	}
	if *flagPin != "" {
		cfg.Pins = append(cfg.Pins, *flagPin)
	}
	cfg.Pinentry = *flagPinentry

	return cfg
}

func Init() error {
	flags.Parse(os.Args[1:])
	return nil
}
