package config

import (
	"fmt"
	"os"
)

const (
	DEBUG_FILE = "/tmp/pinentry-auto.debug.log"
)

func (cfg *Config) DebugLog(s string, things ...interface{}) {
	if cfg.DebugSocket == nil {
		return
	}
	fmt.Fprintf(cfg.DebugSocket, s+"\n", things...)
}

func (cfg *Config) DebugMode() *os.File {
	f, err := os.Create(DEBUG_FILE)
	if err != nil {
		panic(fmt.Sprintf("Could not create debug log: %v", err))
	}
	return f
}
