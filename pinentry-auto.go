package main

import (
	"fmt"
	"log"

	"./config"
	"./pinauto"
)

func mainErr() error {
	if err := config.Init(); err != nil {
		return fmt.Errorf("config.Init: %v", err)
	}

	return pinauto.Start(config.New())
}

func main() {
	if err := mainErr(); err != nil {
		log.Fatalf("ERROR: %v", err)
	}
}
